"use strict";

const root = document.querySelector("#root");
const URL = "https://ajax.test-danit.com/api/swapi/";

class Requests {
    constructor(url) {
        this.url = url;
    }

    get(entity = "") {
        return fetch(this.url + entity)
            .then(response => response.json());
    }
}

class StarWars {
    constructor(baseURL, target) {
        this.target = target;
        this.request = new Requests(baseURL);
    }

    #getFilms() {
        return this.request.get("films");
    }

    #getCharacters(urls) {
        const promises = urls.map(url => {
            const getCharacter = new Requests(url);
            return getCharacter.get();
        })

        return Promise.all(promises)
            .then(responses => responses.map(result => result.name))
            .then(data => this.#renderCharacters(data));
    }

    #renderFilms(data) {
        let elements = data;

        const list = document.createElement("ul");
        list.classList.add("filmsList");

        const listItems = elements.map(({episodeId, name, openingCrawl, characters}) => {
            const li = document.createElement("li");
            li.classList.add("filmItem");

            const loader = document.createElement("div");
            loader.classList.add("lds-ellipsis");
            loader.innerHTML = `<div></div><div></div><div></div><div></div>`;

            li.insertAdjacentHTML("afterbegin", `
                    <h2 class="filmTitle">${episodeId} - ${name}:</h2>
                    <p class="filmDesc">${openingCrawl}</p>
            `);
            li.append(loader);

            loader.style.display = "block";
            this.#getCharacters(characters).then(data => {
                loader.style.display = "none";

                li.insertAdjacentHTML("beforeend", "<h4>Characters:</h4>");
                li.append(data);
            });

            return li;
        })

        list.append(...listItems);

        return list;
    }

    #renderCharacters(data) {
        let elements = data;
        const list = document.createElement("ul");
        list.classList.add("charactersList");

        const listItems = elements.map(el => {
            const li = document.createElement("li");
            li.textContent = el;
            return li;
        })
        list.append(...listItems);

        return list;
    }

    render() {
        this.#getFilms().then(data => this.target.append(this.#renderFilms(data)));
    }
}


const starWars = new StarWars(URL, root);
starWars.render();

